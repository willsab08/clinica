from authApp.models.mascotas import Mascotas
from rest_framework import serializers
class MascotasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mascotas
        fields = ['namePet', 'speciePet', 'genrePet','borndatePet','ownerPet','isActive']