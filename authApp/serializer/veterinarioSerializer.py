from rest_framework import serializers
from authApp.models.veterinario import Veterinario

class VeterinarioSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Veterinario
        fields = ['id', 'username', 'password']

    def create(self, validated_data):        
        VeterinarioInstance = Veterinario.objects.create(**validated_data)        
        return VeterinarioInstance

    def to_representation(self, obj):
        veterinario = Veterinario.objects.get(id=obj.id)        
        return {
            'id': veterinario.id,
            'username': veterinario.username,             
        }