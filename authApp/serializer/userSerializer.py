from rest_framework import serializers
from authApp.models.user import User
from authApp.models.mascotas import Mascotas
from authApp.serializer.mascotasSerializer import MascotasSerializer

class UserSerializer(serializers.ModelSerializer):
    mascotas = MascotasSerializer()
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email', 'mascotas']

    def create(self, validated_data):
        mascotaData = validated_data.pop('mascotas')
        userInstance = User.objects.create(**validated_data)
        Mascotas.objects.create(user=userInstance, **mascotaData)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        mascotas = Mascotas.objects.get(user=obj.id)
        return {
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'email': user.email,
            'mascotas': {
                'id': mascotas.id,
                'namePet': mascotas.namePet,
                'speciePet': mascotas.speciePet,
                'genrePet': mascotas.genrePet,
                'borndatePet': mascotas.borndatePet,
                'ownerPet': mascotas.ownerPet,
                'isActive': mascotas.isActive
            }
        }