from rest_framework import serializers
from authApp.models.historial import Historial

class HistorialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Historial
        fields = ['id', 'fecha', 'consulta','idvet','idmascotas']