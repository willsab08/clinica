from django.db import models
from .user import User

class Mascotas(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='mascotas', on_delete=models.CASCADE)
    namePet = models.CharField(max_length=30)
    speciePet = models.CharField(max_length=30)
    genrePet = models.CharField(max_length=6)
    borndatePet = models.DateTimeField()
    ownerPet = models.CharField(max_length=30)
    isActive = models.BooleanField(default=True)