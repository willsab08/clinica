from django.db import models
from authApp.models.mascotas import Mascotas
from authApp.models.veterinario import Veterinario


class Historial(models.Model):
    id = models.AutoField(primary_key=True)
    fecha = models.DateTimeField()
    consulta = models.CharField(max_length=4000)
    idvet=models.ForeignKey(Veterinario, related_name='id', on_delete=models.CASCADE)
    idmascotas=models.ForeignKey(Mascotas, related_name='id', on_delete=models.CASCADE)