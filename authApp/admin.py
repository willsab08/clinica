from django.contrib import admin

from .models.user import User
from .models.mascotas import Mascotas
from .models.veterinario import Veterinario
from .models.historial import Historial

admin.site.register(User)
admin.site.register(Mascotas)
admin.site.register(Veterinario)
admin.site.register(Historial)
# Register your models here.
